module e-commerce

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/iris-contrib/go.uuid v2.0.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.8.0
)
