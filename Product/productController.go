package Product

import (
	util "e-commerce/Util"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	uuid "github.com/iris-contrib/go.uuid"
)

var GetAllProduct = func(w http.ResponseWriter, r *http.Request) {
	var (
		products []Product
		error    util.Error
		response util.Response
	)

	products, error = GetProduct()

	if (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	response.Status = true
	response.Message = util.DataNotFound

	if len(products) > 0 {
		response.Message = util.Success

		for _, product := range products {
			response.Data = append(response.Data, product)
		}
	}

	util.Respond(w, util.Message(response))
}

var CreateProduct = func(w http.ResponseWriter, r *http.Request) {
	var (
		product  *Product
		error    util.Error
		response util.Response
	)

	auth := r.Header.Get("Authorization")

	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	validator := ProductValidator{Product: product}
	if error = validator.ValidateCreate(auth); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	product.Create(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusCreated)
}

var PatchProduct = func(w http.ResponseWriter, r *http.Request) {
	var (
		product  *Product
		response util.Response
	)

	// Get variables from URI.
	vars := mux.Vars(r)
	productId := vars["ProductId"]
	auth := r.Header.Get("Authorization")

	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	product.ProductId, _ = uuid.FromString(productId)

	validator := ProductValidator{Product: product}
	if error := validator.ValidatePatch(auth); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	product.Patch(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusOK)
}

var DeleteProduct = func(w http.ResponseWriter, r *http.Request) {
	var (
		response util.Response
		product  *Product
	)

	// Get variables from URI.
	vars := mux.Vars(r)
	productId := vars["ProductId"]
	auth := r.Header.Get("Authorization")

	product.ProductId, _ = uuid.FromString(productId)

	validator := ProductValidator{Product: product}
	if error := validator.ValidateDelete(auth); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	product.Delete(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusOK)
}
