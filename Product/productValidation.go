package Product

import (
	util "e-commerce/Util"
)

type ProductValidator struct {
	*Product
}

func (product *ProductValidator) ValidateCreate(auth string) util.Error {
	if result, _ := util.CheckAdmin(auth); !result {
		return util.Error{
			Message: "Only Admin can Create Product",
		}
	}

	if product.ProductName == "" || product.Quantitie == 0 || product.Price == 0 {
		return util.Error{
			Message: "Mandatory Input Should not be empty",
		}
	}

	return util.Error{}
}

func (product *ProductValidator) ValidatePatch(auth string) util.Error {
	if result, _ := util.CheckAdmin(auth); !result {
		return util.Error{
			Message: "Only Admin can Create Product",
		}
	}

	return util.Error{}
}

func (product *ProductValidator) ValidateDelete(auth string) util.Error {
	if result, _ := util.CheckAdmin(auth); !result {
		return util.Error{
			Message: "Only Admin can Create Product",
		}
	}

	return util.Error{}
}
