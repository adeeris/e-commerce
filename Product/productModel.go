package Product

import (
	"database/sql"
	database "e-commerce/Database"
	util "e-commerce/Util"
	"errors"
	"fmt"
	"log"

	uuid "github.com/iris-contrib/go.uuid"
)

type Product struct {
	ProductId   uuid.UUID `json:"ProductId"`
	ProductName string    `json:"ProductName"`
	Quantitie   uint16    `json:"Quantitie"`
	Price       uint64    `json:"Price"`
}

var conn = database.Connection()

func GetProduct() ([]Product, util.Error) {
	products := make([]Product, 0)
	error := util.Error{}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetProduct)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return products, error
	}
	defer rows.Close()

	for rows.Next() {
		record, err := scanProduct(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return products, error
		}

		products = append(products, record)
	}
	return products, error
}

func GetProductById(productId uuid.UUID) (Product, util.Error) {
	product := Product{}
	error := util.Error{}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetProductById, productId)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return product, error
	}
	defer rows.Close()

	for rows.Next() {
		product, err = scanProduct(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return product, error
		}
	}

	return product, error
}

func scanProduct(rows *sql.Rows) (record Product, err error) {
	switch err = rows.Scan(
		&record.ProductId,
		&record.ProductName,
		&record.Quantitie,
		&record.Price,
	); err {
	case sql.ErrNoRows:
		err = errors.New("No User Found")

	case nil:
		record = record

	default:
		err = errors.New("Error when Scan data ")
	}

	return record, nil
}

func (product *Product) Create(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	product.ProductId, _ = uuid.NewV4()

	if _, error = conn.Connection.UseDB().Exec(util.InserQueryProduct, product.ProductId, product.ProductName, product.Quantitie, product.Price); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"ProductId": product.ProductId})
}

func (product *Product) Patch(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	productMap, err := validateStruct(product)

	if err != "" {
		customErr.Message = err
		response.Errors = append(response.Errors, customErr)

		return
	}

	query, values := ComposeQueryUpdateProduct(productMap, product.ProductId)

	if _, error = conn.Connection.UseDB().Exec(query, values...); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"ProductId": product.ProductId})
}

func (product *Product) Delete(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	if _, error = conn.Connection.UseDB().Exec(util.DeleteQueryProduct, product.ProductId); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"ProductId": product.ProductId})
}

func ComposeQueryUpdateProduct(productMap map[string]interface{}, productID uuid.UUID) (string, []interface{}) {
	var (
		query  string
		values []interface{}
	)
	count := 1

	query = util.UpdateQueryProduct

	for key, value := range productMap {
		if count == 1 {
			query += fmt.Sprintf(` "%s" = $%d`, key, count)
			count++
			values = append(values, value)
		} else {
			query += fmt.Sprintf(`, "%s" = $%d`, key, count)
			count++
			values = append(values, value)
		}
	}

	query += fmt.Sprintf(` WHERE "ProductId" = $%d`, count)
	values = append(values, productID)

	return query, values
}

func validateStruct(product *Product) (map[string]interface{}, string) {
	productMap := map[string]interface{}{}

	if product.ProductName != "" {
		productMap["ProductName"] = product.ProductName
	}

	if product.Quantitie > 0 {
		productMap["Quantitie"] = product.Quantitie
	}

	if product.Price > 0 {
		productMap["Price"] = product.Price
	}

	if len(productMap) == 0 {
		return productMap, "Nothing to Update"
	}

	return productMap, ""
}
