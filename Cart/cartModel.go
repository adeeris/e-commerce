package Cart

import (
	"database/sql"
	database "e-commerce/Database"
	product "e-commerce/Product"
	user "e-commerce/User"
	util "e-commerce/Util"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	uuid "github.com/iris-contrib/go.uuid"
)

type Cart struct {
	CartId     uuid.UUID `json:"CartId"`
	ProductId  uuid.UUID `json:"ProductId"`
	UserId     uuid.UUID `json:"UserId"`
	CheckoutTs uint64    `json:"CheckoutTs"`
	Quantitie  uint16    `json:"Quantitie"`
	Price      uint64    `json:"Price"`
	Address    string    `json:"Address"`
	Status     uint8     `json:"Status"`
}

var StatusMap = map[uint8]string{
	1: "Checkout",
	2: "Paid",
}

var conn = database.Connection()

func GetCart() ([]Cart, util.Error) {
	carts := make([]Cart, 0)
	error := util.Error{}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetCart)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return carts, error
	}
	defer rows.Close()

	for rows.Next() {
		record, err := scanCart(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return carts, error
		}

		carts = append(carts, record)
	}
	return carts, error
}

func GetCartById(cartId uuid.UUID) (Cart, util.Error) {
	cart := Cart{}
	error := util.Error{}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetCartById, cartId)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return cart, error
	}
	defer rows.Close()

	for rows.Next() {
		cart, err = scanCart(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return cart, error
		}
	}
	return cart, error
}

func GetCartByStatus(statusArr map[string][]string) ([]Cart, util.Error) {
	var status uint64
	carts := make([]Cart, 0)
	error := util.Error{}

	for _, valueArr := range statusArr {
		for _, value := range valueArr {
			status, _ = strconv.ParseUint(value, 10, 64)
		}
	}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetCartByStatus, status)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return carts, error
	}
	defer rows.Close()

	for rows.Next() {
		record, err := scanCart(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return carts, error
		}

		carts = append(carts, record)
	}
	return carts, error
}

func scanCart(rows *sql.Rows) (record Cart, err error) {
	switch err = rows.Scan(
		&record.CartId,
		&record.ProductId,
		&record.UserId,
		&record.CheckoutTs,
		&record.Quantitie,
		&record.Price,
		&record.Address,
		&record.Status,
	); err {
	case sql.ErrNoRows:
		err = errors.New("No Cart Found")

	case nil:
		record = record

	default:
		err = errors.New("Error when Scan data ")
	}

	return record, nil
}

func (cart *Cart) Create(response *util.Response) {
	var (
		customErr   util.Error
		error       error
		prod        product.Product
		currentUser user.User
	)

	cart.CartId, _ = uuid.NewV4()

	if prod, customErr = product.GetProductById(cart.ProductId); (customErr != util.Error{}) {
		response.Errors = append(response.Errors, customErr)

		return
	}

	if currentUser, customErr = user.GetUserById(cart.UserId); (customErr != util.Error{}) {
		response.Errors = append(response.Errors, customErr)

		return
	}

	if cart.Address == "" {
		cart.Address = currentUser.Address
	}

	cart.CheckoutTs = uint64(time.Now().Unix())
	cart.Price = prod.Price * uint64(cart.Quantitie)

	if _, error = conn.Connection.UseDB().Exec(util.InserQueryCart, cart.CartId, cart.ProductId, cart.UserId, cart.CheckoutTs, cart.Quantitie, cart.Price, cart.Address, cart.Status); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"CartId": cart.CartId})
}

func (cart *Cart) Delete(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	if _, error = conn.Connection.UseDB().Exec(util.DeleteQueryCart, cart.CartId); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"CartId": cart.CartId})
}

func (cart *Cart) Patch(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	cartMap, err := validateStruct(cart)

	if err != "" {
		customErr.Message = err
		response.Errors = append(response.Errors, customErr)

		return
	}

	query, values := ComposeQueryUpdateCart(cartMap, cart.CartId)

	if _, error = conn.Connection.UseDB().Exec(query, values...); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"CartId": cart.CartId})
}

func ComposeQueryUpdateCart(cartMap map[string]interface{}, cartID uuid.UUID) (string, []interface{}) {
	var (
		query  string
		values []interface{}
	)
	count := 1

	query = util.UpdateQueryCart

	for key, value := range cartMap {
		if count == 1 {
			query += fmt.Sprintf(` "%s" = $%d`, key, count)
			count++
			values = append(values, value)
		} else {
			query += fmt.Sprintf(`, "%s" = $%d`, key, count)
			count++
			values = append(values, value)
		}
	}

	query += fmt.Sprintf(` WHERE "CartId" = $%d`, count)
	values = append(values, cartID)

	return query, values
}

func validateStruct(cart *Cart) (map[string]interface{}, string) {
	cartMap := map[string]interface{}{}

	if cart.Address != "" {
		cartMap["Address"] = cart.Address
	}

	if cart.Status != 0 {
		cartMap["Status"] = cart.Status

		if cart.Status == 2 {
			err := patchProdStatusPaid(cart.ProductId, cart.CartId)

			if err != "" {
				return nil, err
			}
		}
	}

	if cart.Quantitie != 0 {
		var (
			prod      product.Product
			customErr util.Error
		)

		cartMap["Quantitie"] = cart.Quantitie

		if prod, customErr = product.GetProductById(cart.ProductId); (customErr != util.Error{}) {

			return nil, customErr.Message
		}

		cartMap["Price"] = prod.Price * uint64(cart.Quantitie)

	}

	return cartMap, ""
}

func patchProdStatusPaid(productId, cartId uuid.UUID) string {
	var (
		prod        product.Product
		customErr   util.Error
		currentCart Cart
		response    util.Response
	)

	if prod, customErr = product.GetProductById(productId); (customErr != util.Error{}) {

		return customErr.Message
	}

	if currentCart, customErr = GetCartById(cartId); (customErr != util.Error{}) {

		return customErr.Message
	}

	prod.Quantitie = prod.Quantitie - currentCart.Quantitie

	prod.Patch(&response)

	if len(response.Errors) != 0 {

		return response.Message
	}

	return ""
}
