package Cart

import (
	util "e-commerce/Util"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	uuid "github.com/iris-contrib/go.uuid"
)

var GetAllCart = func(w http.ResponseWriter, r *http.Request) {
	var (
		carts    []Cart
		error    util.Error
		response util.Response
	)

	carts, error = GetCart()

	if (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	response.Status = true
	response.Message = util.DataNotFound

	if len(carts) > 0 {
		response.Message = util.Success

		for _, cart := range carts {
			response.Data = append(response.Data, cart)
		}
	}

	util.Respond(w, util.Message(response))
}

var GetAllCartByStatus = func(w http.ResponseWriter, r *http.Request) {
	var (
		carts    []Cart
		error    util.Error
		response util.Response
	)

	query := r.URL.Query()

	carts, error = GetCartByStatus(query)

	if (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	response.Status = true
	response.Message = util.DataNotFound

	if len(carts) > 0 {
		response.Message = util.Success

		for _, cart := range carts {
			response.Data = append(response.Data, cart)
		}
	}

	util.Respond(w, util.Message(response))
}

var CreateCart = func(w http.ResponseWriter, r *http.Request) {
	var (
		cart     *Cart
		error    util.Error
		response util.Response
	)

	productId := r.Header.Get("ProductId")
	userId := r.Header.Get("UserId")

	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	cart.ProductId, _ = uuid.FromString(productId)
	cart.UserId, _ = uuid.FromString(userId)

	validator := CartValidator{Cart: cart}
	if error = validator.ValidateCreate(); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	cart.Create(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusCreated)
}

var DeleteCart = func(w http.ResponseWriter, r *http.Request) {
	var response util.Response

	cart := Cart{}

	// Get variables from URI.
	vars := mux.Vars(r)
	cartId := vars["CartId"]

	cart.CartId, _ = uuid.FromString(cartId)

	cart.Delete(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusOK)
}

var PatchCart = func(w http.ResponseWriter, r *http.Request) {
	var (
		cart     *Cart
		response util.Response
	)

	// Get variables from URI.
	vars := mux.Vars(r)
	cartId := vars["CartId"]

	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	cart.CartId, _ = uuid.FromString(cartId)

	cart.Patch(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusOK)
}
