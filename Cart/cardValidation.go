package Cart

import (
	util "e-commerce/Util"

	uuid "github.com/iris-contrib/go.uuid"
)

type CartValidator struct {
	*Cart
}

func (cart *CartValidator) ValidateCreate() util.Error {
	if cart.ProductId == (uuid.UUID{}) || cart.UserId == (uuid.UUID{}) {
		return util.Error{
			Message: "Mandatory Input Should not be empty",
		}
	}

	if cart.Quantitie == 0 {
		return util.Error{
			Message: "Quantitie cannot be 0",
		}
	}

	return util.Error{}
}

func (cart *CartValidator) ValidatePatch() util.Error {
	if _, ok := StatusMap[cart.Status]; !ok {
		return util.Error{
			Message: "Status not Valid",
		}
	}

	return util.Error{}
}
