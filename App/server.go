package App

import (
	"log"
	"net/http"
	"os"
)

func Launch() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Println("Launch in port ", port)
	// Server
	err := http.ListenAndServe(":"+port, router)

	if err != nil {
		log.Fatal("Failed to lunch" + err.Error())
	}
}
