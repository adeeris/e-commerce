package App

import (
	util "e-commerce/Util"
	"fmt"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
)

func verifyToken(r *http.Request) (token *jwt.Token, err error) {
	auth := r.Header.Get("Authorization")
	if auth == "" {
		return nil, fmt.Errorf("Authorization is nil")
	}
	token, err = jwt.Parse(auth, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected Method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("SECRET_ACCESS")), nil
	})

	if err != nil {
		return nil, err
	}

	return token, nil
}

var TokenValidity = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			response util.Response
		)

		error := util.Error{}
		token, err := verifyToken(r)
		if err != nil {
			error.Message = err.Error()
			response.Errors = append(response.Errors, error)
			util.Respond(w, util.Message(response))
		}

		if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
			error.Message = err.Error()
			response.Errors = append(response.Errors, error)
			util.Respond(w, util.Message(response))
		}

		next.ServeHTTP(w, r)
	})
}
