package App

import (
	cart "e-commerce/Cart"
	product "e-commerce/Product"
	user "e-commerce/User"
	"net/http"

	"github.com/gorilla/mux"
)

var router *mux.Router

func SetRoutes() {
	router = mux.NewRouter()

	router.HandleFunc("/login", user.Login).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc("/user", user.CreateUser).Methods(http.MethodPost, http.MethodOptions)

	v1 := router.PathPrefix("/v/1").Subrouter()
	v1.Use(TokenValidity)

	v1.HandleFunc("/product", product.GetAllProduct).Methods(http.MethodGet, http.MethodOptions)
	v1.HandleFunc("/product", product.CreateProduct).Methods(http.MethodPost, http.MethodOptions)
	v1.HandleFunc("/product/{ProductId}", product.PatchProduct).Methods(http.MethodPatch, http.MethodOptions)
	v1.HandleFunc("/product/{ProductId}", product.DeleteProduct).Methods(http.MethodDelete, http.MethodOptions)

	v1.HandleFunc("/cart", cart.GetAllCart).Methods(http.MethodGet, http.MethodOptions)
	v1.HandleFunc("/cartStatus", cart.GetAllCartByStatus).Methods(http.MethodGet, http.MethodOptions)
	v1.HandleFunc("/cart", cart.CreateCart).Methods(http.MethodPost, http.MethodOptions)
	v1.HandleFunc("/cart/{CartId}", cart.PatchCart).Methods(http.MethodPatch, http.MethodOptions)
	v1.HandleFunc("/cart/{CartId}", cart.DeleteCart).Methods(http.MethodDelete, http.MethodOptions)
}
