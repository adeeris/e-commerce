package User

import (
	"database/sql"
	database "e-commerce/Database"
	util "e-commerce/Util"
	"errors"
	"fmt"
	"log"

	uuid "github.com/iris-contrib/go.uuid"
)

type User struct {
	UserId   uuid.UUID `json:"UserId"`
	Username string    `json:"Username"`
	Email    string    `json:"Email"`
	Password string    `json:"Password"`
	Address  string    `json:"Address"`
	Role     uint8     `json:"Role"`
}

var roleMap = map[uint8]string{
	1: "Admin",
	2: "User",
}

var conn = database.Connection()

func (user *User) Create(response *util.Response) {
	var (
		customErr util.Error
		error     error
	)

	user.UserId, _ = uuid.NewV4()

	if _, error = conn.Connection.UseDB().Exec(util.InserQuery, user.UserId, user.Username, user.Email, user.Password, user.Address, user.Role); error != nil {
		customErr.Message = error.Error()
		response.Errors = append(response.Errors, customErr)

		return
	}

	response.Data = append(response.Data, map[string]interface{}{"UserId": user.UserId})
	log.Println(fmt.Println("Success Create user with userId %+v", user.UserId))

}

func GetUser(user *User) (*User, util.Error) {
	var error util.Error

	currentUser := User{}

	rows, err := conn.Connection.UseDB().Query(util.QueryLogin, user.Email, user.Password)
	if err != nil {
		log.Println("Error when Get data : ", err)
		error.Message = err.Error()

		return &currentUser, error
	}

	defer rows.Close()

	for rows.Next() {
		currentUser, err = scanUser(rows)
		if err != nil {
			log.Println("error when scan User", err)
			error.Message = err.Error()

			return &currentUser, error
		}
	}

	return &currentUser, error
}

func GetUserById(userId uuid.UUID) (User, util.Error) {
	user := User{}
	error := util.Error{}

	rows, err := conn.Connection.UseDB().Query(util.QueryGetUserById, userId)

	if err != nil {
		log.Println("Error when Get data : ", err)

		error.Message = err.Error()

		return user, error
	}
	defer rows.Close()

	for rows.Next() {
		user, err = scanUser(rows)
		if err != nil {
			log.Println(err)

			error.Message = err.Error()

			return user, error
		}
	}
	return user, error
}

func scanUser(rows *sql.Rows) (record User, err error) {
	switch err = rows.Scan(
		&record.UserId,
		&record.Username,
		&record.Email,
		&record.Password,
		&record.Address,
		&record.Role,
	); err {
	case sql.ErrNoRows:
		err = errors.New("No User Found")

	case nil:
		record = record

	default:
		err = errors.New("Error when Scan data ")
	}

	return record, nil
}
