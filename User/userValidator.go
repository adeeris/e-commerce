package User

import util "e-commerce/Util"

type UserValidator struct {
	*User
}

func (user *UserValidator) ValidateLogin() util.Error {
	if user.Username == "" && user.Password == "" {
		return util.Error{
			Message: "Mandatory Input Should not be empty",
		}
	}

	return util.Error{}
}

func (user *UserValidator) ValidateCreate() util.Error {
	if user.Email == "" || user.Password == "" || user.Username == "" {
		return util.Error{
			Message: "Mandatory Input Should not be empty",
		}
	}

	if _, ok := roleMap[user.Role]; !ok {
		return util.Error{
			Message: "Role not Valid",
		}
	}

	return util.Error{}
}
