package User

import (
	"e-commerce/Authentication"
	util "e-commerce/Util"
	"encoding/json"
	"log"
	"net/http"
)

var CreateUser = func(w http.ResponseWriter, r *http.Request) {
	var (
		user     *User
		error    util.Error
		response util.Response
	)

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	validator := UserValidator{User: user}
	if error = validator.ValidateCreate(); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	user.Create(&response)

	response.Status = true
	response.Message = util.Success

	if len(response.Errors) != 0 {
		response.Status = false
		response.Message = util.ProblemRequest
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	util.Respond(w, util.Message(response), http.StatusCreated)
}

var Login = func(w http.ResponseWriter, r *http.Request) {
	var (
		user        *User
		error       util.Error
		response    util.Response
		currentUser *User
	)

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	validator := UserValidator{User: user}
	if error = validator.ValidateLogin(); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	if currentUser, error = GetUser(user); (util.Error{}) != error {
		response.Message = error.Message
		response.Errors = append(response.Errors, error)
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	token, err := Authentication.GenerateToken(currentUser.UserId, currentUser.Role)

	if err != nil {
		log.Println("Error when Generate Token : ", err)
		response.Message = util.ProblemRequest
		response.Errors = append(response.Errors, util.Error{
			Message: err.Error(),
		})
		util.Respond(w, util.Message(response), http.StatusBadRequest)

		return
	}

	response.Status = true
	response.Message = util.Success
	response.Data = append(response.Data, token)

	util.Respond(w, util.Message(response), http.StatusOK)
}
