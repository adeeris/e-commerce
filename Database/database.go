package Database

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
)

type Conn struct {
	Connection *Connect
}

//Connect set up connecton using sql.DB
type Connect struct {
	db *sql.DB
}

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

//ConnectToDB make a connection to postgresql
func ConnectToDB(rdbms, dbURL string) (conn *Connect, err error) {
	fmt.Println("Connecting to ", dbURL)

	db, err := sql.Open(rdbms, dbURL)
	if err != nil {
		fmt.Println("Error Connecting to DB: ", err)
	}

	if db.Ping(); err != nil {
		log.Println("Error when Ping DB : ", err)
		db.Close()
		return
	}

	conn = &Connect{db: db}
	// defer db.Close()

	return conn, nil
}

func (conn *Connect) UseDB() *sql.DB {
	return conn.db
}

//Connection set up the connection to DB
func Connection() (connection *Conn) {
	var (
		con *Connect
		err error
	)

	rdbms := os.Getenv("DB")
	host := os.Getenv("HOST")
	port := os.Getenv("PORTDB")
	user := os.Getenv("USER")
	dbname := os.Getenv("DBNAME")
	password := os.Getenv("PASSWORD")
	dbURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", host, port, user, dbname, password)

	con, err = ConnectToDB(rdbms, dbURL)

	if err != nil {
		log.Fatal("Error Connect to DB", err.Error())
	}

	return &Conn{Connection: con}
}
