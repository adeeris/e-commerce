package Util

import (
	"fmt"
	"os"

	"github.com/dgrijalva/jwt-go"
)

func CheckAdmin(authToken string) (bool, error) {
	token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected Method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("SECRET_ACCESS")), nil
	})

	if err != nil {
		return false, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		role, ok := claims["Role"].(string)
		if !ok {
			return false, err
		}

		if role != "Admin" {
			return false, err
		}
	}
	return true, nil
}
