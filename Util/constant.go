package Util

const (
	ProblemRequest     = "Problem Request"
	RequestMalfunction = "Request MalFunction"
	FailGetUser        = "Fail to Get User"
	QueryMalfunction   = "Query Mulfunction"
	Success            = "Success"
	InvalidInput       = "Invalid Input"
	DataNotFound       = "Data Not Found"

	QueryLogin           = `Select * from public."User" where "Email" = $1 and "Password" = $2`
	QueryGetProduct      = `Select * from public."Product"`
	QueryGetCart         = `Select * from public."Cart"`
	QueryGetCartByStatus = `Select * from public."Cart" WHERE "Status" = $1`
	QueryGetCartById     = `Select * from public."Cart" WHERE "CartId" = $1`
	InserQuery           = `INSERT INTO "User"("UserId", "Username", "Email","Password", "Address", "Role")
	VALUES ($1,$2,$3,$4,$5,$6)`
	InserQueryProduct = `INSERT INTO "Product"("ProductId", "ProductName", "Quantitie","Price")
	VALUES ($1,$2,$3,$4)`
	InserQueryCart = `INSERT INTO "Cart"("CartId","ProductId", "UserId", "CheckoutTs","Quantitie", "Price", "Address", "Status")
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`
	UpdateQueryProduct = `UPDATE public."Product" SET `
	UpdateQueryCart    = `UPDATE public."Cart" SET `
	DeleteQueryProduct = `DELETE FROM public."Product" where "ProductId" = $1`
	DeleteQueryCart    = `DELETE FROM public."Cart" where "CartId" = $1`

	QueryGetProductById = `Select * from public."Product" WHERE "ProductId" = $1`
	QueryGetUserById    = `Select * from public."User" WHERE "UserId" = $1`
)
