package Util

import (
	"encoding/json"
	"net/http"
	"reflect"
	"time"
)

type Response struct {
	Status  bool
	Message string
	Data    []interface{}
	Errors  []Error `json:"Error"`
}

type Error struct {
	Message string `json:"message"`
}

func objectToArray(data interface{}) interface{} {
	if reflect.ValueOf(data).IsNil() {
		return make([]interface{}, 0)
	}

	return data
}

func Message(response Response) map[string]interface{} {
	return map[string]interface{}{
		"status":  response.Status,
		"message": response.Message,
		"data":    objectToArray(response.Data),
		"error":   objectToArray(response.Errors),
	}
}

func Respond(w http.ResponseWriter, data map[string]interface{}, httpStatusCode ...int) {
	w.Header().Add("Content-Type", "application/json")

	if len(httpStatusCode) > 0 {
		w.WriteHeader(httpStatusCode[0])
	}

	json.NewEncoder(w).Encode(data)
}

func GetJSON(url string, target interface{}) error {
	var client = &http.Client{Timeout: 15 * time.Second}

	result, err := client.Get(url)

	if err != nil {
		return err
	}

	defer result.Body.Close()

	return json.NewDecoder(result.Body).Decode(target)
}
