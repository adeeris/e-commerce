package main

import (
	app "e-commerce/App"
	"log"

	"github.com/joho/godotenv"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	app.SetRoutes()
	app.Launch()
}
