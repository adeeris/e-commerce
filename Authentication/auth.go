package Authentication

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	uuid "github.com/iris-contrib/go.uuid"
)

func GenerateToken(usersub uuid.UUID, role uint8) (string, error) {
	var err error
	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["Usersub"] = usersub
	atClaims["Role"] = role
	atClaims["exp"] = time.Now().Add(time.Minute * 60).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("SECRET_ACCESS")))
	if err != nil {
		return "", err
	}
	return token, nil
}
